
#include "Signal.hpp"
#include <iostream>
using namespace sl;

// void f(int i) { std::cout << "f(" << i << ")\n"; }

class Counter : public Object {
public:
  Counter(std::string name) : m_value{0}, name{name} {
    valueChanged.connect(this, &Counter::show);
  }
  void display() { std::cout << name << "-count:" << m_value; }
  void setValue(int v) {
    if (v != m_value) {
      m_value = v;
      valueChanged(v);
    }
  }
  void show(int i) { std::cout << "show(" << i << ")\n"; }
  Signal<int> valueChanged;

private:
  int m_value;
  std::string name;
};

class Test {
public:
  Signal<int> go;
};

// void f(int i) { std::cout << "fonction globale appelée lorsque la valeur de a
// change : " << i << "\n"; }

void f(int &i) { i += 10; }

int main() {

  sl::Signal<int &> sig;
  sig.connect(f);
  int a = 2;
  sig(a);
  std::cout << a << std::endl;

  return 0;
}

/* Test t;
 {
   Counter a("A");
   a.valueChanged.connect(f);
   a.setValue(30);
   t.go.connect(&a, &Counter::show);
   t.go(27);
 }

 t.go(35);
}*/
