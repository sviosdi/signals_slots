#include "Object.h"

#include "Signal.hpp"
#include <algorithm>

namespace sl {

Object::~Object() { deleteConnectedSignals(); }

void Object::addSignal(SaSSignal *signal) {
  connectedSignals.push_back(signal);
}

void Object::removeOneSignal(SaSSignal *signal) {
  auto it = std::find(connectedSignals.begin(), connectedSignals.end(), signal);
  if (it != connectedSignals.end())
    connectedSignals.erase(it);
  connectedSignals.erase(it, connectedSignals.end());
}
void Object::removeAllSignals(SaSSignal *signal) {
  auto it =
      std::remove(connectedSignals.begin(), connectedSignals.end(), signal);
  connectedSignals.erase(it, connectedSignals.end());
}

void Object::deleteConnectedSignals() {
  for (SaSSignal *signal : connectedSignals) {
    signal->disconnect(this);
  }
}

} // namespace sl
