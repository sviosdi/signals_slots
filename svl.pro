TEMPLATE = app
CONFIG += console c++20
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        src/Object.cpp \
        src/main.cpp

INCLUDEPATH += include/

HEADERS += \
  include/Object.h \
  include/Signal.hpp \
  include/Slot.hpp
