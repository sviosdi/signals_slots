![Signaux&Slots](/sig.webp)
![cpp20](https://img.shields.io/badge/C%2B%2B-20-yellowgreen) ![licence](https://img.shields.io/badge/licence-GPLv3-blue)
_Mécanisme_ de _signaux et slots_ en C++.<br />
Aucune librairie tierce n'est nécessaire. Seule la _stl_ est utilisée.

Le mécanisme de signaux et de slots permet de connecter un `Signal` (qui est une fonction un peu particulière) à un `Slot` qui est une fonction globale ou une méthode d'une classe, mais ayant les mêmes paramètres que le signal. À chaque _émission_ du signal (qui correspond à l'appel de la fonction signal avec ses paramètres s'il y en a) le slot connecté est effectué avec les paramètres transmis par le signal.

La documention pour l'utilisation et pour l'explication du code peut être consultée ici [svl:signals&slots](https://sviosdi.github.io/svl-doc/docs/category/signaux--slots)
