/*------------------------------------------------------------------------------
    SavvyLite: Signal.hpp
    Signal à connecter à une fonction ou à une méthode.
    Licence: GPLv3
    sviosdi - dernière mise à jour le 16.12.2022
-------------------------------------------------------------------------------*/

#ifndef SVL_SIGNAL_HPP
#define SVL_SIGNAL_HPP

#include <iostream>
#include <map>
#include <type_traits>

#include "Object.h"
#include "Slot.hpp"

namespace sl {
/*--------------------------------------------------------------------------------
                           SaSSignal
 ---------------------------------------------------------------------------------*/
class SaSSignal {
public:
  SaSSignal() {}

public:
  virtual void disconnect(void *obj_an) = 0;
};

/*--------------------------------------------------------------------------------
                           Signal
 ---------------------------------------------------------------------------------*/
template <typename... Params> class Signal : private SaSSignal {
public:
  Signal() : connectedSlots{} {}

  virtual ~Signal() { disconnectAll(); }

  template <typename OBJ_APPELE, typename RT>
  void connect(OBJ_APPELE *obj_appele, RT (OBJ_APPELE::*methode)(Params...)) {
    SlotKey key(reinterpret_cast<void *>(obj_appele),
                reinterpret_cast<char *>(&methode));
    if (connectedSlots.count(key))
      return;
    SlotMethode<OBJ_APPELE, RT, Params...> slot;
    if (std::is_base_of<Object, OBJ_APPELE>::value) {
      ((Object *)obj_appele)->addSignal(this);
      slot.isObject = true;
    }
    connectedSlots[key] = slot;
  }

  template <typename RT> void connect(RT (*function)(Params...)) {
    SlotKey key(reinterpret_cast<void *>(function));
    if (connectedSlots.count(key))
      return;
    SlotFunction<RT, Params...> slot;
    connectedSlots[key] = slot;
  }

  void operator()(Params... params) {
    for (auto &[key, value] : connectedSlots) {
      Slot<Params...> &slot = value;
      slot(key, params...);
    }
  }

  template <typename OBJ_APPELE, typename RT>
  void disconnect(OBJ_APPELE *obj_appele,
                  RT (OBJ_APPELE::*methode)(Params...)) {
    SlotKey key(reinterpret_cast<void *>(obj_appele),
                reinterpret_cast<char *>(&methode));
    if (connectedSlots.count(key))
      connectedSlots.erase(key);
    if (std::is_base_of<Object, OBJ_APPELE>::value) {
      ((Object *)obj_appele)->removeOneSignal(this);
    }
  }

  template <typename RT> void disconnect(RT (*function)(Params...)) {
    SlotKey key(reinterpret_cast<void *>(function));
    connectedSlots.erase(key);
  }

  template <typename OBJ_APPELE> void disconnect(OBJ_APPELE *obj) {
    typename std::map<SlotKey, Slot<Params...>>::iterator itr =
        connectedSlots.begin();
    while (itr != connectedSlots.end()) {
      if (itr->first.obj_anonyme == obj) {
        itr = connectedSlots.erase(itr);
      } else {
        ++itr;
      }
    }
    if (std::is_base_of<Object, OBJ_APPELE>::value) {
      ((Object *)obj)->removeAllSignals(this);
    }
  }

  void disconnect(void *obj_an) override {
    typename std::map<SlotKey, Slot<Params...>>::iterator itr =
        connectedSlots.begin();
    while (itr != connectedSlots.end()) {
      if (itr->first.obj_anonyme != nullptr) {
        if (itr->first.obj_anonyme == obj_an) {
          itr = connectedSlots.erase(itr);
        } else {
          ++itr;
        }
      } else
        ++itr;
    }
    ((Object *)obj_an)->removeAllSignals(this);
  }

  void disconnectAll() {
    typename std::map<SlotKey, Slot<Params...>>::iterator itr =
        connectedSlots.begin();
    while (itr != connectedSlots.end()) {
      if (itr->second.isObject) {
        ((Object *)itr->first.obj_anonyme)->removeOneSignal(this);
      }
      ++itr;
    }
    connectedSlots.clear();
  }

private:
  std::map<SlotKey, Slot<Params...>> connectedSlots;
};

} // namespace sl

#endif // SVL_SIGNAL_HPP
