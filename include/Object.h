/*------------------------------------------------------------------------------
    SavvyLite: Object.h
    Classe de base de Sl. Permet la déconnexion automatique des signaux
    connectés à une méthode d'une des classes qui héritent d'elle.
    Licence: GPLv3
    sviosdi - dernière mise à jour le 16.12.2022
-------------------------------------------------------------------------------*/
#ifndef SVL_OBJECT_H
#define SVL_OBJECT_H

#include <iostream>
#include <vector>

namespace sl {

class SaSSignal;

class Object {
public:
  virtual ~Object();
  void addSignal(SaSSignal *signal);
  void removeOneSignal(SaSSignal *signal);
  void removeAllSignals(SaSSignal *signal);
  void deleteConnectedSignals();

private:
  std::vector<SaSSignal *>
      connectedSignals; // conteneur de tous les signaux connectés à une méthode
                        // de cet objet.
};

} // namespace sl
#endif
