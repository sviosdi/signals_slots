/*------------------------------------------------------------------------------
    SavvyLite: Slot.hpp
    Slot pour connexion à un Signal.
    Licence: GPLv3
    sviosdi - dernière mise à jour le 16.12.2022
-------------------------------------------------------------------------------*/

#ifndef SVL_SLOT_HPP
#define SVL_SLOT_HPP

/*
 * PTR_METH_SIZE: taille en octet d'un pointeur de méthode. Lancer les tests
 * après compilation Ajuster cette valeur selon la plateforme cible si
 * nécessaire.
 */
constexpr short PTR_METH_SIZE = 16;

namespace sl {

/*------------------------------------------------------------------------------
                     SlotKey
  -------------------------------------------------------------------------------*/
struct SlotKey {
  SlotKey() : obj_anonyme{nullptr}, ptr_methode{nullptr} {}

  SlotKey(void *obj, char *meth) : obj_anonyme{obj} {
    char *tmp = meth;
    ptr_methode = new char[PTR_METH_SIZE];
    for (int i = 0; i < PTR_METH_SIZE; i++)
      ptr_methode[i] = tmp[i];
  }

  SlotKey(void *func) : obj_anonyme{nullptr}, ptr_function{func} {}

  SlotKey(const SlotKey &other) {
    if (other.obj_anonyme == nullptr) {
      obj_anonyme = nullptr;
      ptr_function = other.ptr_function;
    } else {
      obj_anonyme = other.obj_anonyme;
      ptr_methode = new char[PTR_METH_SIZE];
      for (int i = 0; i < PTR_METH_SIZE; i++)
        ptr_methode[i] = other.ptr_methode[i];
    }
  }

  ~SlotKey() {
    if (obj_anonyme) {
      if (ptr_methode) {
        delete[] ptr_methode;
        ptr_methode = nullptr;
      }
    }
  }

  /* ----------------------------------------------------------------------------------------------------------------------
   Ordre sur les SlotKeys :
    • Sl_fonction ~ Sl_fonction : on compare directement les deux pointeurs de
    • fonctions. Sl_fonction ~ Sl_methode  : on considère dans tous les cas que
  : • fonction < methode. Sl_methode1 ~ Sl_methode2 : dans l'ordre de objets et
    • si les objets sont égaux dans l'ordre des pointeurs de méthode.
  ------------------------------------------------------------------------------------------------------------------------*/
  friend short int compareSlots(const SlotKey &a, const SlotKey &b) {
    if (a.obj_anonyme == nullptr) {   // a fonction
      if (b.obj_anonyme == nullptr) { // a fonction - b fonction
        if (a.ptr_function < b.ptr_function)
          return -1;
        if (a.ptr_function == b.ptr_function)
          return 0;
        return 1;
      } else { // a fonction - b methode
        return -1;
      }
    } else {                          // a methode
      if (b.obj_anonyme == nullptr) { // b fonction
        return 1;
      } else { // b methode
        if (a.obj_anonyme < b.obj_anonyme)
          return -1;
        if (a.obj_anonyme > b.obj_anonyme)
          return 1;
        // return compare(a.ptr_methode, b.ptr_methode);
        if (*(a.ptr_methode) < *(b.ptr_methode))
          return -1;
        if (*(a.ptr_methode) > *(b.ptr_methode))
          return 1;
        return 0; // égalité des methodes*/
      }
    }
  }

  friend bool operator==(const SlotKey &l, const SlotKey &r) {
    short b = compareSlots(l, r);
    return b == 0;
  }

  friend bool operator!=(const SlotKey &l, const SlotKey &r) {
    short b = compareSlots(l, r);
    return b != 0;
  }

  friend bool operator<=(const SlotKey &l, const SlotKey &r) {
    short b = compareSlots(l, r);
    return (b == -1 || b == 0);
  }

  friend bool operator>=(const SlotKey &l, const SlotKey &r) {
    short b = compareSlots(l, r);
    return (b == 1 || b == 0);
  }

  friend bool operator<(const SlotKey &l, const SlotKey &r) {
    short b = compareSlots(l, r);
    return b == -1;
  }

  friend bool operator>(const SlotKey &l, const SlotKey &r) {
    short b = compareSlots(l, r);
    return b == 1;
  }

  void *obj_anonyme;
  union {
    char *ptr_methode{nullptr};
    const void *ptr_function;
  };
};

/*------------------------------------------------------------------------------
                     Slot
  -------------------------------------------------------------------------------*/
template <typename... Params> class Slot {
public:
  Slot() {}
  Slot(void (*fct_appel)(const SlotKey &, Params...))
      : appel{fct_appel}, isObject{false} {}

  void operator()(const SlotKey &key, Params... params) {
    appel(key, params...);
  }

  bool operator!=(const Slot &other) { return appel != other.appel; }

  void (*appel)(const SlotKey &, Params...);
  bool isObject;
};

/*------------------------------------------------------------------------------
                     SlotMethode
  -------------------------------------------------------------------------------*/
template <typename OBJ_APPELE, typename RT, typename... Params>
class SlotMethode : public Slot<Params...> {
public:
  SlotMethode() : Slot<Params...>(&appel) {}

  static void appel(const SlotKey &key, Params... params) {
    using TYPE_PTR_METHODE = RT (OBJ_APPELE::*)(Params... params);
    OBJ_APPELE *objet_appele = reinterpret_cast<OBJ_APPELE *>(key.obj_anonyme);
    TYPE_PTR_METHODE methode =
        *(reinterpret_cast<TYPE_PTR_METHODE *>(key.ptr_methode));
    (objet_appele->*methode)(params...);
  }
};

/*------------------------------------------------------------------------------
                     SlotFunction
  -------------------------------------------------------------------------------*/
template <typename RT, typename... Params>
class SlotFunction : public Slot<Params...> {
public:
  SlotFunction() : Slot<Params...>(&appel) {}
  static void appel(const SlotKey &key, Params... params) {
    using TYPE_FUNC = RT (*)(Params...);
    (reinterpret_cast<TYPE_FUNC>(key.ptr_function))(params...);
  }
};

} // namespace sl

#endif // SVL_SLOT_HPP
